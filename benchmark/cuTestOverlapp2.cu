#include <Box2D/Box2D.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <float.h>
#include <limits.h>
#include <iostream>
#include "cuTestOverlapp.h"

#define IDX2C(i,j,ld) (((j)*(ld))+(i))

#define DIM 2

float* h_2S;

long long k_time;
long long m_time;


#define CUDA_ERROR_CHECK
 
#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )
 
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
#ifdef CUDA_ERROR_CHECK
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
#endif
 
    return;
}

inline void __cudaCheckError( const char *file, const int line )
{
    cudaError err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
 
    // More careful checking. However, this will affect performance.
    // Comment away if needed.
    err = cudaDeviceSynchronize();
    if( cudaSuccess != err )
    {
        fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
                 file, line, cudaGetErrorString( err ) );
        exit( -1 );
    }
 
    return;
}

void init_cuTestOverlap()
{
	cudaError_t cudaStat;
	cublasStatus_t stat;
	cublasHandle_t handle;

	m_time = 0, k_time = 0;
	//identity matrix
	float* I = (float *) malloc(16*sizeof(float));
	memset(I, 0, 16*sizeof(float));
	for(int i = 0; i < DIM; ++i)
			I[IDX2C(i,i,4)] = 1;



	h_2S = (float *) malloc(4*DIM*DIM*sizeof(float));
	memset(h_2S, 0, 4*DIM*DIM*sizeof(float));

	float* d_2S;
	cudaStat = cudaMalloc((void **)&d_2S, 4*DIM*DIM*sizeof(float));
	if(cudaStat != cudaSuccess)
	{
		printf ("device memory allocation failed\n");
		return ;
	}

	stat = cublasCreate(&handle);
	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("CUBLAS init failed\n");
		return ;
	}

	stat = cublasSetMatrix(DIM, DIM, sizeof(float), I, 2*DIM, d_2S, 2*DIM);

	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("data download failed\n");
		cudaFree(d_2S);
		cublasDestroy(handle);
		return ;
	}

	for(int i = 0; i < DIM; ++i)
			I[IDX2C(i,i,2*DIM)] = -1;

	stat = cublasSetMatrix(DIM, DIM, sizeof(float), I, 2*DIM, &d_2S[IDX2C(0,DIM,2*DIM)], 2*DIM);


	float* d_1V;
	cudaStat = cudaMalloc((void **)&d_1V, 2*DIM*sizeof(float));
	for(int i = 0; i < 2*DIM; ++i)
		I[i] = 1;

	stat = cublasSetVector(2*DIM, sizeof(float), I, 1, d_1V, 1); 

	cudaMemcpy2D(&d_2S[IDX2C(DIM,0,2*DIM)], 2*DIM*sizeof(float), &d_2S[IDX2C(0,DIM,2*DIM)], 2*DIM*sizeof(float), DIM*sizeof(float), DIM, cudaMemcpyDeviceToDevice);
	cudaMemcpy2D(&d_2S[IDX2C(DIM,DIM,2*DIM)], 2*DIM*sizeof(float), d_2S, 2*DIM*sizeof(float), DIM*sizeof(float), DIM, cudaMemcpyDeviceToDevice);

	float alpha = -2.0, beta = 0.0;
	cublasSscal(handle, 4*DIM*DIM, &alpha, d_2S, 1);

	alpha = 1.0;
	stat = cublasSgemv(handle, CUBLAS_OP_N, DIM, DIM, &alpha, d_2S, 2*DIM, d_1V, 1, &beta, d_1V, 1);
	
	stat = cublasGetMatrix(2*DIM, 2*DIM, sizeof(float), d_2S, 2*DIM, h_2S, 2*DIM);
	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("data upload failed\n");
		cudaFree(d_2S);
		cublasDestroy(handle);
		return ;
	}
}

int findPivot(float *P, int departed, int entering, int basics, int pW, int pH)
{
			float ratio = FLT_MAX;
			int minrow = pH;
			
			for(int i = 0; i < pH; ++i)
			{	
					float m = P[IDX2C(i, pW-1, pH)];
					if(m < 0 && m < ratio)
					{
							ratio = m;
							minrow = i;
					}
			}

			if(minrow == pH)
			{
				for(int i = 0; i < pH; ++i)
				{
					float m = P[IDX2C(i, entering, pH)];
					if(m > 0)
					{
						float r = P[IDX2C(i, pW-1, pH)]/m;
						if(r < ratio)
						{
								ratio = r;
								minrow = i;
						}
					}
				}
			}
					
			if(minrow == pH) {return -1;}
			else return minrow;
}	

__global__
void LCP(float* P, float*P2, int entering, int minrow, int pW, int pH)
{
			int tid = blockIdx.x*blockDim.x + threadIdx.x;
			if(tid >= pW)
					return;

			float *col = &P2[pH*tid];
			if(tid != entering)
			{
				for(int i = 0; i < pH; ++i)
				{
					col[i] = P[IDX2C(i, tid, pH)];

					if(i != minrow)
					{
						col[i] -= (P[IDX2C(i, entering, pH)] * P[IDX2C(minrow, tid, pH)] / P[IDX2C(minrow, entering, pH)]);
					}
					else
					{
						col[i] /= (P[IDX2C(minrow, entering, pH)]);
					}
				}
			}
			else
			{
				for(int i = 0; i < pH; ++i)
					if(i != minrow)
					{
						col[i] = 0;
					}
					else
					{
						col[i] = 1;
					}
			}
}

bool cuTestOverlap(const b2PolygonShape* shapeA, int32 indexA,
								const b2PolygonShape* shapeB, int32 indexB,
								const b2Transform& xfa, const b2Transform& xfb)
{
		int v, nv1, nv2, con, basics;
		int pH, pW;
		b2Vec2 p1, p2;

		nv1 = shapeA->GetVertexCount();
		nv2 = shapeB->GetVertexCount();
		con = nv1 + nv2;
		basics = (4 + con + 1);
		pH = basics;
		pW = 2*basics + 1;


		float32 tx, ty;


		float *P = (float *) malloc(pH*pW*sizeof(float)); 
		memset(P, 0, pH*pW*sizeof(float));

		float *q = &P[IDX2C(0,pW-1,pH)];
		memset(q, 0, 5*sizeof(float));

		for(int i = 0; i < pH; ++i)
		{
			P[IDX2C(i,i,pH)] = 1;
			P[IDX2C(i,basics,pH)] = -1;
		}

		for(int i = 0; i < 2*DIM; ++i)
		{
			for(int j = 0; j < 2*DIM; ++j)
			{
				P[IDX2C(i+1,j+pH+1,pH)] = h_2S[IDX2C(i,j,2*DIM)];
			}
		}

		int i = 0, j = 5;

		p1 = shapeA->GetVertex(0);
		tx = xfa.p.x, ty = xfa.p.y;
		for(v = 1; v < nv1; ++v)
		{
				b2Vec2 p2 = shapeA->GetVertex(v);

				float dy = p2.y - p1.y;
				float dx = p2.x - p1.x;

				P[IDX2C(5+i,pH+1,pH)] = dy;
				P[IDX2C(5+i,pH+2,pH)] = -dx;
				P[IDX2C(1,5+pH+i,pH)] = -dy;
				P[IDX2C(2,5+pH+i,pH)] = dx;

				q[j] = -dx*(p2.y + ty) + dy*(p2.x + tx);
				++i, ++j;

				p1 = p2;
		}	
	
		{
				b2Vec2 p2 = shapeA->GetVertex(0);

				float dy = p2.y - p1.y;
				float dx = p2.x - p1.x;

				P[IDX2C(5+i,pH+1,pH)] = dy;
				P[IDX2C(5+i,pH+2,pH)] = -dx;
				P[IDX2C(1,5+pH+i,pH)] = -dy;
				P[IDX2C(2,5+pH+i,pH)] = dx;

				q[j] = -dx*(p2.y + ty) + dy*(p2.x + tx);
				++i, ++j;

				p1 = p2;	
		}

		p1 = shapeB->GetVertex(0);
		tx = xfb.p.x, ty = xfb.p.y;
		for(v = 1; v < nv2; ++v)
		{
				b2Vec2 p2 = shapeB->GetVertex(v);

				float dy = p2.y - p1.y;
				float dx = p2.x - p1.x;

				P[IDX2C(5+i,pH+3,pH)] = dy;
				P[IDX2C(5+i,pH+4,pH)] = -dx;
				P[IDX2C(3,5+pH+i,pH)] = -dy;
				P[IDX2C(4,5+pH+i,pH)] = dx;

				q[j] = -dx*(p2.y + ty) + dy*(p2.x + tx);
				++i, ++j;

				p1 = p2;
		}	
	
		{
				b2Vec2 p2 = shapeB->GetVertex(0);

				float dy = p2.y - p1.y;
				float dx = p2.x - p1.x;

				P[IDX2C(5+i,pH+3,pH)] = dy;
				P[IDX2C(5+i,pH+4,pH)] = -dx;
				P[IDX2C(3,5+pH+i,pH)] = -dy;
				P[IDX2C(4,5+pH+i,pH)] = dx;

				q[j] = -dx*(p2.y + ty) + dy*(p2.x + tx);
				++i, ++j;

				p1 = p2;	
		}

		int *basic = (int *) malloc((2*basics)*sizeof(int));
		for(int i = 0; i < basics; ++i)
		{
			basic[i] = i;
		}


		int departed = 0;

		long long time = gettime();
		float *d_P;
		float *d_P2;
		cudaMalloc((void **)&d_P, pW*pH*sizeof(float));
		cudaMalloc((void **)&d_P2, pW*pH*sizeof(float));
	
		cudaMemcpy(d_P, P, pW*pH*sizeof(float), cudaMemcpyHostToDevice);

		int blocks = (pW + 1024)/1024;
		int tpb = 1024;
		int iter = 0;
		while(departed != basics && iter < 20)
		{
			long long time2 = gettime(); 
			int entering = (departed + basics) % (2*basics);
			int pivot = findPivot(P, departed, entering, basics, pW, pH);
			if(pivot == -1) break;
			LCP<<<blocks, tpb, 0>>>(d_P, d_P2, entering, pivot, pW, pH);	
			float *tmp = d_P;
			d_P = d_P2;
			d_P2 = tmp;
			departed = basic[pivot];
			basic[pivot] = entering;
			cudaMemcpy(P, d_P, pW*pH*sizeof(float), cudaMemcpyDeviceToHost);
			iter++;
		}
			/////
			//CudaCheckError();	
			/*
			printf("\n%d\n", departed);

			cudaMemcpy(basic, d_basic, basics*sizeof(int), cudaMemcpyDeviceToHost);
			printf("\n\n\n");
			for(int i = 0; i < basics; ++i)
				printf("%d,", basic[i]);

			printf("\n%d\n", departed);

			printf("\n\n\n");
			for(int i = 0; i < pH; ++i)
			{
				for(int j = 0; j < pW; ++j)
				{
					printf("%3.0f", P[IDX2C(i,j,pH)]);
				}
				printf("\n");
			}
			*/

//		fflush(stdout);
		bool overlap = false;
		
		if(departed == basics)
		{
		int ix1 = -1, iy1 = -1, ix2 = -1, iy2 = -1;

		for(int i = 0; i < basics; ++i)
		{
			if(basic[i] == basics+1) ix1 = i;
			if(basic[i] == basics+2) iy1 = i;
			if(basic[i] == basics+3) ix2 = i;
			if(basic[i] == basics+4) iy2 = i;
		}

		float x1 = (ix1 >= 0)?(P[IDX2C(ix1, pW-1, pH)]):(0);
		float y1 = (iy1 >= 0)?(P[IDX2C(iy1, pW-1, pH)]):(0);
		float x2 = (ix2 >= 0)?(P[IDX2C(ix2, pW-1, pH)]):(0);
		float y2 = (iy2 >= 0)?(P[IDX2C(iy2, pW-1, pH)]):(0);
		overlap = (abs(x2 - x1) < b2_epsilon);
	 	overlap = (abs(y2 - y1) < b2_epsilon);
		}	

		free(P);
		free(basic);
		cudaFree(d_P);

		time = gettime() - time;
		return overlap;
}
