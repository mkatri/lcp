#include <sys/time.h>
void init_cuTestOverlap();

bool cuTestOverlap(const b2PolygonShape* shapeA, int32 indexA, 
				           const b2PolygonShape* shapeB, int32 indexB,
				           const b2Transform& xfa, const b2Transform& xfb);

inline long long gettime()
{
	struct timeval  tv;
	gettimeofday(&tv, NULL);

	long long time_in_micro = 
					         (tv.tv_sec) * 1000000 + (tv.tv_usec);
	return time_in_micro;
}


