#include <Box2D/Box2D.h>
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <sys/time.h>
#include <algorithm>
#include <set>
#include <vector>
#include <math.h>
#include "cuTestOverlapp.h"

#define TEST_SIZE 10
#define W_MAX_X	400	
#define W_MAX_Y	400 
#define O_MAX_D 40 
#define O_MIN_D 10 

b2PolygonShape shapes[TEST_SIZE];
b2BodyDef defs[TEST_SIZE];
b2Body *bodies[TEST_SIZE];

b2Vec2 *vertices;

void generateVertices(int nvertices)
{

	double r = rand()%O_MAX_D + O_MIN_D;
	double start = ((double)rand())*(2*3.14159265359)/RAND_MAX;
	for(int i = 0; i < nvertices; i++)
	{
			double angle = (2*3.14159265359)/nvertices;
		 	angle *= i;
			angle += start; 
			vertices[i] = b2Vec2(r*cos(angle), r*sin(angle)); 
	}	
}

int main(int argc, char** argv)
{
	B2_NOT_USED(argc);
	B2_NOT_USED(argv);

	srand(time(NULL));
	// Define the gravity vector.
	b2Vec2 gravity(0.0f, -10.0f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	b2World world(gravity);

	init_cuTestOverlap();

	int nvertices;
	if(argc > 1) nvertices = atoi(argv[1]); 
	else nvertices = 3;	

	printf("%d\n", nvertices);
	vertices = (b2Vec2 *) malloc(nvertices*sizeof(b2Vec2));

	for(int i = 0; i < TEST_SIZE; ++i)
	{
		generateVertices(nvertices);
		defs[i].position.Set(rand()%W_MAX_X, rand()%W_MAX_Y);
		bodies[i] = world.CreateBody(&defs[i]);
		//shapes[i].SetAsBox(rand()%O_MAX_D, rand()%O_MAX_D);
		shapes[i].Set(vertices, nvertices);
		bodies[i]->CreateFixture(&shapes[i], 0.0f);
	}

	printf("DONE GENERATING\n");

	int touching = 0;
	int i = 0, j = i+1, c = 0;

	long long ts = gettime();
	for(int i = 0; i < TEST_SIZE; ++i)
		for(int j = i+1; j < TEST_SIZE; j++)
		{
			bool t1 = b2TestOverlap(&shapes[i], 0, &shapes[j], 0, bodies[i]->GetTransform(), bodies[j]->GetTransform());
		}

	printf("tested in %lld\n", gettime()-ts);

	ts = gettime();
	for(int i = 0; i < TEST_SIZE; ++i)
		for(int j = i+1; j < TEST_SIZE; j++)
		{
			bool t2 = cuTestOverlap(&shapes[i], 0, &shapes[j], 0, bodies[i]->GetTransform(), bodies[j]->GetTransform());
		}
	printf("tested in %lld\n", gettime()-ts);

	/*
	vertices[0] = b2Vec2(0, 0);
	vertices[1] = b2Vec2(4, 0);
	vertices[2] = b2Vec2(2, 2);

	defs[0].position.Set(0, 0);
	bodies[0] = world.CreateBody(&defs[0]);
	shapes[0].Set(vertices, 3);
	bodies[0]->CreateFixture(&shapes[0], 0.0f);

	defs[1].position.Set(0, 1);
	bodies[1] = world.CreateBody(&defs[1]);
	shapes[1].Set(vertices, 3);
	bodies[1]->CreateFixture(&shapes[1], 0.0f);


	int touching = cuTestOverlap(&shapes[0], 0, &shapes[1], 0, bodies[0]->GetTransform(), bodies[1]->GetTransform());

	*/
	/*
	for(int i = 0; i < TEST_SIZE; ++i)
		for(int j = i+1; j < TEST_SIZE; j++)
		{
			touching += cuTestOverlap(&shapes[i], 0, &shapes[j], 0, bodies[i]->GetTransform(), bodies[j]->GetTransform());
		}
*/

	//printf("%d\n", touching);
/*
	//XXX
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f, 4.0f);
	b2Body* groundBody = world.CreateBody(&groundBodyDef);
	//XXX BODY > FIXTURE > SHAPE, BODY HAS TRANSFORM
	b2PolygonShape groundBox;
	groundBox.SetAsBox(50.0f, 10.0f);
	groundBody->CreateFixture(&groundBox, 0.0f);


	//XXX
	b2BodyDef bodyDef;
	bodyDef.position.Set(0.0f, 4.0f);
	b2Body* body = world.CreateBody(&bodyDef);
	//XXX BODY > FIXTURE > SHAPE, BODY HAS TRANSFORM
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(1.0f, 1.0f);
	body->CreateFixture(&dynamicBox, 0.0f);
	bool touching = b2TestOverlap(&groundBox, 0, &dynamicBox, 0, groundBody->GetTransform(), body->GetTransform());

	printf("%s\n", touching ? "true" : "false");
*/
	return 0;
}
