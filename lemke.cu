#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <float.h>
#include <limits.h>

#define DIM 2
#define PNT 2
#define CON 3

#define IDX2C(i,j,ld) (((j)*(ld))+(i))

#define CONV CON*PNT
#define	DIMV DIM*PNT 
#define BASICS (DIM*PNT + CON*PNT)
#define PDIM1 (BASICS + 1)
#define PDIM2 (2*PDIM1 + 1)


__global__
void LCP(double* P, int *basic, int *departed)
{

			int entering = (*departed + BASICS + 1) % (2*(BASICS + 1));
			int tid = blockIdx.x*blockDim.x + threadIdx.x;

			double ratio = DBL_MAX;
			int minrow = PDIM1;
			
			for(int i = 0; i < 	PDIM1; ++i)
			{	
					double m = P[IDX2C(i, PDIM2-1, PDIM1)];
					if(m < 0 && m < ratio)
					{
							ratio = m;
							minrow = i;
					}
			}

			if(minrow == PDIM1)
			{
				for(int i = 0; i < PDIM1; ++i)
				{
					double m = P[IDX2C(i, entering, PDIM1)];
					if(m > 0)
					{
						double r = P[IDX2C(i, PDIM2-1, PDIM1)]/m;
						if(r < ratio)
						{
								ratio = r;
								minrow = i;
						}
					}
				}
			}
					
			if(minrow == PDIM1) return;
		
			double col[PDIM1];

			if(tid != entering)
			{
				for(int i = 0; i < PDIM1; ++i)
				{
					col[i] = P[IDX2C(i, tid, PDIM1)];

					if(i != minrow)
					{
						col[i] -= (P[IDX2C(i, entering, PDIM1)] * P[IDX2C(minrow, tid, PDIM1)] / P[IDX2C(minrow, entering, PDIM1)]);
					}
					else
					{
						col[i] /= (P[IDX2C(minrow, entering, PDIM1)]);
					}
				}
			}
			else
			{
				for(int i = 0; i < PDIM1; ++i)
					if(i != minrow)
					{
						col[i] = 0;
					}
					else
					{
						col[i] = 1;
					}
			}

			__syncthreads();

			for(int i = 0; i < PDIM1; ++i)
				P[IDX2C(i, tid, PDIM1)] = col[i];

			__syncthreads();
			if(tid == 0){
					*departed = basic[minrow];
					basic[minrow] = entering;
			}

}


int main()
{

	cudaError_t cudaStat;
	cublasStatus_t stat;
	cublasHandle_t handle;

	//identity matrix
	double* I = (double *) malloc(4*DIM*DIM*sizeof(double));
	memset(I, 0, 4*DIM*DIM*sizeof(double));
	for(int i = 0; i < DIM; ++i)
			I[IDX2C(i,i,2*DIM)] = 1;

	//A
	double* A = (double *) malloc(DIM*PNT*CON*PNT*sizeof(double));
	memset(A, 0, DIM*PNT*CON*PNT*sizeof(double));
	A[IDX2C(0, 0, CON*PNT)] = -1;
 	A[IDX2C(0, 1, CON*PNT)] = 1;
	A[IDX2C(1, 0, CON*PNT)] = 1;
	A[IDX2C(1, 1, CON*PNT)] = 1;
	A[IDX2C(2, 0, CON*PNT)] = 0;
	A[IDX2C(2, 1, CON*PNT)] = -1;

	A[IDX2C(3, 2, CON*PNT)] = -1;
	A[IDX2C(3, 3, CON*PNT)] = 1;
	A[IDX2C(4, 2, CON*PNT)] = 1;
	A[IDX2C(4, 3, CON*PNT)] = 1;
	A[IDX2C(5, 2, CON*PNT)] = 0;
	A[IDX2C(5, 3, CON*PNT)] = -1;

	//Q
	double* q = (double *) malloc((DIM*PNT + CON*PNT)*sizeof(double));
	memset(q, 0, (DIM*PNT + CON*PNT)*sizeof(double));
	q[4] = 0;
	q[5] = 4;
	q[6] = 0;
	
	q[7] = 1;
	q[8] = 5;
	q[9] = -1;

	//Z
	double* z = (double *) malloc((BASICS+1)*sizeof(double));
	memset(z, 0, (BASICS+1)*sizeof(double));

	double* w = (double *) malloc((BASICS+1)*sizeof(double));

	int *basic = (int *) malloc((2*(BASICS + 1))*sizeof(int));
	int *nonbasic = &basic[BASICS+1];

	for(int i = 0; i < BASICS + 1; ++i)
	{
		basic[i] = i;
		nonbasic[i] = i + BASICS + 1;
	}


	//z0 == minimum in q
	z[0] = 1;
	w[0] = z[0];

	for(int i = 1; i < BASICS+1; ++i)
		w[i] = z[0] + q[i-1];

	//z0 just became basic
	//w5 just became nonbasic

	int departing = basic[5]; 
//	basic[5] = nonbasic[0];
//	nonbasic[0] = departing;	


	double* P = (double *) malloc(PDIM1*PDIM2*sizeof(double));
	memset(P, 0, PDIM1*PDIM2*sizeof(double));

	double* h_2S;
	h_2S = (double *) malloc(4*DIM*DIM*sizeof(double));
	memset(h_2S, 0, 4*DIM*DIM*sizeof(double));

	double* d_2S;
	cudaStat = cudaMalloc((void **)&d_2S, 4*DIM*DIM*sizeof(double));
	if(cudaStat != cudaSuccess)
	{
		printf ("device memory allocation failed\n");
		return EXIT_FAILURE;
	}

	stat = cublasCreate(&handle);
	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("CUBLAS init failed\n");
		return EXIT_FAILURE;
	}

	stat = cublasSetMatrix(DIM, DIM, sizeof(double), I, 2*DIM, d_2S, 2*DIM);

	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("data download failed\n");
		cudaFree(d_2S);
		cublasDestroy(handle);
		return EXIT_FAILURE;
	}

	for(int i = 0; i < DIM; ++i)
			I[IDX2C(i,i,2*DIM)] = -1;

	stat = cublasSetMatrix(DIM, DIM, sizeof(double), I, 2*DIM, &d_2S[IDX2C(0,DIM,2*DIM)], 2*DIM);


	double* d_1V;
	cudaStat = cudaMalloc((void **)&d_1V, 2*DIM*sizeof(double));
	for(int i = 0; i < 2*DIM; ++i)
		I[i] = 1;

	stat = cublasSetVector(2*DIM, sizeof(double), I, 1, d_1V, 1); 

	cudaMemcpy2D(&d_2S[IDX2C(DIM,0,2*DIM)], 2*DIM*sizeof(double), &d_2S[IDX2C(0,DIM,2*DIM)], 2*DIM*sizeof(double), DIM*sizeof(double), DIM, cudaMemcpyDeviceToDevice);
	cudaMemcpy2D(&d_2S[IDX2C(DIM,DIM,2*DIM)], 2*DIM*sizeof(double), d_2S, 2*DIM*sizeof(double), DIM*sizeof(double), DIM, cudaMemcpyDeviceToDevice);

	double alpha = 2.0, beta = 0.0;
	cublasDscal(handle, 4*DIM*DIM, &alpha, d_2S, 1);

	alpha = -1.0;
	stat = cublasDgemv(handle, CUBLAS_OP_N, DIM, DIM, &alpha, d_2S, 2*DIM, d_1V, 1, &beta, d_1V, 1);
	
	stat = cublasGetMatrix(2*DIM, 2*DIM, sizeof(double), d_2S, 2*DIM, h_2S, 2*DIM);
	if(stat != CUBLAS_STATUS_SUCCESS)
	{
		printf("data upload failed\n");
		cudaFree(d_2S);
		cublasDestroy(handle);
		return EXIT_FAILURE;
	}

	for(int i = 0; i < PDIM1; ++i)
	{
		P[IDX2C(i,i,PDIM1)] = 1;
	}

	for(int i = 0; i < 2*DIM; ++i)
	{
		for(int j = 0; j < 2*DIM; ++j)
		{
				P[IDX2C(i+1,j+PDIM1+1,PDIM1)] = -h_2S[IDX2C(i,j,2*DIM)];
		}
	}

	for(int i = 0; i < CONV; ++i)
	{
		for(int j = 0; j < DIMV; ++j)
		{
				P[IDX2C(j+1, i+PDIM1+2*DIM+1, PDIM1)] = -A[IDX2C(i,j,CONV)];
				P[IDX2C(i+1+2*DIM, j+PDIM1+1, PDIM1)] = A[IDX2C(i,j,CONV)];
		}
	}

	for(int i = 0; i < PDIM1; ++i)
		P[IDX2C(i, PDIM1, PDIM1)] = -1;

	for(int i = 0; i < BASICS; ++i)
		P[IDX2C(i+1, PDIM2-1, PDIM1)] = q[IDX2C(i,0,BASICS)];

	for(int i = 0; i < PDIM1; ++i)
	{
		for(int j = 0; j < PDIM2; ++j)
		{
			printf("%3.0f", P[IDX2C(i,j,PDIM1)]);
		}
		printf("\n");
	}

	departing = 0;

	int *d_departed;
	int *d_basic;
	double *d_P;
	cudaMalloc((void **)&d_P, PDIM1*PDIM2*sizeof(double));
	cudaMalloc((void **)&d_departed, sizeof(int));
	cudaMalloc((void **)&d_basic, (BASICS+1)*sizeof(int));
	
	cudaMemcpy(d_P, P, PDIM1*PDIM2*sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_basic, basic, (BASICS+1)*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_departed, &departing, sizeof(int), cudaMemcpyHostToDevice);

	printf("%d\n", departing);
	printf("\n\n\n");
	for(int i = 0; i < BASICS+1; ++i)
		printf("%d,", basic[i]);

	printf("\n%d\n", departing);

	printf("\n\n\n");
	for(int i = 0; i < PDIM1; ++i)
	{
		for(int j = 0; j < PDIM2; ++j)
		{
			printf("%3.0f", P[IDX2C(i,j,PDIM1)]);
		}
		printf("\n");
	}


	while(departing != BASICS + 1)
	{
		LCP<<<1, PDIM2, 0>>>(d_P, d_basic, d_departed);	
		
	cudaMemcpy(P, d_P, PDIM1*PDIM2*sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(basic, d_basic, (BASICS+1)*sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(&departing, d_departed, sizeof(int), cudaMemcpyDeviceToHost);


	printf("\n\n\n");
	for(int i = 0; i < BASICS+1; ++i)
		printf("%d,", basic[i]);

	printf("\n%d\n", departing);

	printf("\n\n\n");
	for(int i = 0; i < PDIM1; ++i)
	{
		for(int j = 0; j < PDIM2; ++j)
		{
			printf("%3.0f", P[IDX2C(i,j,PDIM1)]);
		}
		printf("\n");
	}
	}

	cudaFree(d_2S);
	cublasDestroy(handle);
	
	free(I);
	return EXIT_SUCCESS;
}	

